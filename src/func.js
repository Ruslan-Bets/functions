const getSum = (str1, str2) => {
  if (isNaN(str1) || Array.isArray(str1) || isNaN(str2) || Array.isArray(str2)) { return false }
  str1 = str1 === '' ? 0 : parseFloat(str1)
  str2 = str2 === '' ? 0 : parseFloat(str2)
  return `${str1 + str2}`
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let posts = listOfPosts.filter(post => post.author === authorName).length
  let comment = listOfPosts.reduce((sum, post) => {
    return sum + (post.comments === undefined ? 0 : post.comments.filter(comment => comment.author === authorName).length)
  }, 0)

  return `Post:${posts},comments:${comment}`
}

const tickets=(people)=> {
  
  let ticket = 0;
  for (i = 0; i < people.length - 1; i++){
    people[i] == 25 ? ticket += 25 : ticket -= people[i] - 25;
}
    if (ticket <= 0){
    return 'NO';
  } else {
    return 'YES';
  }
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
